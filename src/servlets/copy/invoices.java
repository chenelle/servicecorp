package servlets.copy;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicecorp.invoice;
import dao.Dao;

public class invoices extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public invoices() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ResultSet rs = Dao.getAll("invoices");
			ArrayList<invoice> allInvoices = new ArrayList<invoice>();
			while (rs.next()) {
				invoice current = new invoice(rs.getInt("order_id"), rs.getFloat("price"), rs.getBoolean("paid"));
				allInvoices.add(current);
			}
			StringBuilder out = new StringBuilder();
			out.append("<table><tr>");
			out.append("<th>Order ID</th>");
			out.append("<th>Price</th>");
			out.append("<th>Paid?</th>");
			
			for (invoice i:allInvoices) {
				out.append(i.toTableRow());
			}
			
			out.append("</table>");
			response.getWriter().append(out.toString());
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}