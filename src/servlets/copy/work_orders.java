package servlets.copy;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicecorp.work_order;
import dao.Dao;

public class work_orders extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public work_orders() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ResultSet rs = Dao.getAll("work_orders");
			ArrayList<work_order> allWorkOrders = new ArrayList<work_order>();
			while (rs.next()) {
				work_order current = new work_order(rs.getInt("work_orders_id"), rs.getDate("order_date"), rs.getInt("order_time"), rs.getString("printer"), rs.getString("location"), rs.getString("description"));
				allWorkOrders.add(current);
			}
			StringBuilder out = new StringBuilder();
			out.append("<table><tr>");
			out.append("<th>Order ID</th>");
			out.append("<th>Order Date</th>");
			out.append("<th>Order Time</th>");
			out.append("<th>Printer Model</th>");
			out.append("<th>Location</th>");
			out.append("<th>Description of Issue</th>");
			
			for (work_order w:allWorkOrders) {
				out.append(w.toTableRow());
			}
			
			out.append("</table>");
			response.getWriter().append(out.toString());
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}
