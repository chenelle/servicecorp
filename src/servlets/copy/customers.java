package servlets.copy;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servicecorp.customer;
import dao.Dao;

public class customers extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public customers() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ResultSet rs = Dao.getAll("customers");
			ArrayList<customer> allCustomers = new ArrayList<customer>();
			while (rs.next()) {
				customer current = new customer(rs.getInt("customer_id"), rs.getString("LastName"), rs.getString("FirstName"), rs.getString("organization"), rs.getString("email"), rs.getInt("PhoneNumber"));
				allCustomers.add(current);
			}
			StringBuilder out = new StringBuilder();
			out.append("<table><tr>");
			out.append("<th>Customer ID</th>");
			out.append("<th>Last Name</th>");
			out.append("<th>First Name</th>");
			out.append("<th>Organization</th>");
			out.append("<th>Email</th>");
			out.append("<th>Phone Number</th>");
			
			for (customer w:allCustomers) {
				out.append(w.toTableRow());
			}
			
			out.append("</table>");
			response.getWriter().append(out.toString());
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}
