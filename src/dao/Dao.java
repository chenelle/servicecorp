package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class Dao {
	private static String uri = "jdbc:postgresql://localhost:5432/servicecorp";
	private static String db_name = "servicecorp";
	private static String username = "user";
	private static String password = "rules";
	
	public static Connection connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(uri, username, password);
			//System.out.println("Connected to PostgreSQL server successfully");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}
	
	public static ResultSet executeQuery(String query) {
		//System.out.println("Begin execute query: " + query);
		ResultSet rs = null;
		try {
			Connection conn = connect();
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			conn.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return rs;
	}
	
	public static ResultSet getAll(String tableName) {
		return executeQuery("SELECT * FROM " + tableName + ";");
	}
}