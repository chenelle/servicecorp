package servicecorp;

public class customer {
	private int customer_id;
	private String LastName;
	private String FirstName;
	private String organization;
	private int PhoneNumber;
	private String email;
	
	public customer(int id, String LastName, String FirstName, String organization, String email, int phone_number) {
		this.set_id(id);
		this.set_last_name(LastName);
		this.set_first_name(FirstName);
		this.set_organization(organization);
		this.set_email(email);
		this.set_phone_number(phone_number);
	}
	
	public void set_id(int id) {
		this.customer_id = customer_id;
	}
	
	public void set_last_name(String last_name) {
		this.LastName = last_name;
	}
	
	public void set_first_name(String first_name) {
		this.FirstName = first_name;
	}
	
	public void set_organization(String organization) {
		this.organization = organization;
	}
	
	public void set_email(String email) {
		this.email = email;
	}
	
	public void set_phone_number(int phone_number) {
		this.PhoneNumber = phone_number;
	}
	
	public int get_id() {
		return this.customer_id;
	}
	
	public String get_last_name() {
		return this.LastName;
	}
	
	public String get_first_name() {
		return this.FirstName;
	}
	
	public String get_organization() {
		return this.organization;
	}
	
	public String get_email() {
		return this.email;
	}
	
	public int get_phone_number() {
		return this.PhoneNumber;
	}
	
	public String toTableRow() {
		StringBuilder out = new StringBuilder();
		out.append("<tr>");
		out.append("<td>" + this.get_id() + "</td>");
		out.append("<td>" + this.get_last_name() + "</td>");
		out.append("<td>" + this.get_first_name() + "</td>");
		out.append("<td>" + this.get_organization() + "</td>");
		out.append("<td>" + this.get_email() + "</td>");
		out.append("<td>" + this.get_phone_number() + "</td>");
		out.append("</tr>");
		return out.toString();
	}

}
