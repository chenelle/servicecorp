
package servicecorp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class work_order {
	private int work_orders_id;
	private Date order_date;
	private int order_time;
	private String printer;
	private String location;
	private String description;

	public work_order(int id, java.sql.Date order_date, int order_time, String printer, String location, String description) {
		this.set_id(id);
		this.set_order_date(order_date);
		this.set_order_time(order_time);
		this.set_printer(printer);
		this.set_location(location);
		this.set_description(description);
	}
	
	/* Figure out how to make the sql date a java date
	private Date _parseSQLDate(java.sql.Date date) {
		if (date == null) {
			return null;
		} 
		else {
			String[] strArr = (String(date)).split("-");
			int[] intArr = new int[strArr.length];
			for (int i = 0; i < strArr.length; i++) {
				intArr[i] = Integer.parseInt(strArr[i]);
		}
		Date cal = new GregorianCalendar(intArr[0], intArr[1], intArr[2]).getTime();
			//cal.set(intArr[0], intArr[1], intArr[2]);
			
		return cal;
		}
	}
	
	*/
	
	public void set_id(int id) {
		this.work_orders_id = id;
	}
	
	public void set_order_date(Date date) {
		this.order_date = date;
	}
	
	public void set_order_time(int time) {
		this.order_time = time;
	}
	
	public void set_printer(String printer) {
		this.printer = printer;
	}
	
	public void set_location(String location) {
		this.location = location;
	}
	
	public void set_description(String description) {
		this.description = description;
	}
	
	public int get_id() {
		return this.work_orders_id;
	}
	
	public Date get_order_date() {
		return this.order_date;
	}
	
	public int get_order_time() {
		return this.order_time;
	}
	
	public String get_printer() {
		return this.printer;
	}
	
	public String get_location() {
		return this.location;
	}
	
	public String get_description() {
		return this.description;
	}
	
	public String toTableRow() {
		StringBuilder out = new StringBuilder();
		out.append("<tr>");
		out.append("<td>" + this.get_id() + "</td>");
		out.append("<td>" + this.get_order_date() + "</td>");
		out.append("<td>" + this.get_order_time() + "</td>");
		out.append("<td>" + this.get_printer() + "</td>");
		out.append("<td>" + this.get_location() + "</td>");
		out.append("<td>" + this.get_description() + "</td>");
		out.append("</tr>");
		return out.toString();
	}

}


